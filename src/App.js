import { Component } from 'react';
import Tabs from './components/Tabs/Tabs';
import Plans from './containers/Plans/Plans';
import Layout from './hoc/Layout/Layout';
import ContainerWrapper from './hoc/ContainerWrapper/ContainerWrapper';
import Characteristics from './containers/Characteristics/Characteristics';

export default class App extends Component {
  state = { currentTab: 0 };

  render() {
    // usar react router em vez desta solução
    const containers = [<Plans />, <Characteristics />];

    return (
      <Layout>
        <Tabs
          tabNames={['Planos', 'Características']}
          currentTab={this.state.currentTab}
          changedTab={(index) => this.setState({ currentTab: index })}
        />
        <ContainerWrapper>{containers[this.state.currentTab]}</ContainerWrapper>
      </Layout>
    );
  }
}
