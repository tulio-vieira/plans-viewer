import { Component } from 'react';
import classes from './NavBar.module.css';

export default class Navbar extends Component {
  render() {
    return (
      <header className={classes.Navbar}>
        <div className={classes.logo}></div>
        <div className={classes.title}>SAMSUNG GALAXY S8</div>
      </header>
    );
  }
}
