// tabNames: [string <tabname>, string <tabName>, ...]
// currentTab: int <index>

import classes from './Tabs.module.css';

export default function Tabs({ tabNames, currentTab, changedTab }) {
  return (
    <div className={classes.Tabs}>
      {tabNames.map((name, i) => (
        <div
          key={i}
          onClick={() => changedTab(i)}
          className={
            classes.tab + ' ' + (i === currentTab ? classes.active : '')
          }
        >
          {name}
        </div>
      ))}
    </div>
  );
}
