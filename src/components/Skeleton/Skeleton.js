// Fork of https://medium.com/octopus-wealth/skeleton-loading-pages-with-react-5a931f12677b

import classes from './Skeleton.module.css';

const Skeleton = ({ amount = 1 }) => {
  return new Array(amount)
    .fill(0)
    .map((_, i) => <div className={classes.Skeleton} key={i} />);
};

export default Skeleton;
