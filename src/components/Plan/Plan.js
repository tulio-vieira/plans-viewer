import classes from './Plan.module.css';
import { numberToPrice } from '../../shared/util';

export default function Plan({
  name,
  monthly_fee,
  phonePrice,
  phonePriceOnPlan,
}) {
  return (
    <div className={classes.Plan}>
      <h1>Plano {name}</h1>
      <table className={classes.pricesTable}>
        <tbody>
          <tr>
            <td>Pós Pago - À Vista</td>
            <td>{numberToPrice(phonePriceOnPlan)}</td>
          </tr>
          <tr>
            <td>Mensalidade total do Plano</td>
            <td>{numberToPrice(monthly_fee)}</td>
          </tr>
          <tr>
            <td>Pré Pago* - À Vista</td>
            <td>{numberToPrice(phonePrice)}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
