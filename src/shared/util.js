export const filterPlans = (plans) => {
  let planSet = {};

  const currentDate = Date.now();

  for (let plan of plans) {
    const planDate = Date.parse(plan.schedule.startDate);
    if (planDate > currentDate) continue;

    if (planSet[plan.name]) {
      const localidadeDoPlanoAtual = plan.localidade.prioridade;
      const localidadeDoPlanoAntigo = planSet[plan.name].localidade.prioridade;

      if (localidadeDoPlanoAtual > localidadeDoPlanoAntigo) {
        planSet[plan.name] = plan;
      } else if (
        localidadeDoPlanoAtual === localidadeDoPlanoAntigo &&
        planDate > Date.parse(planSet[plan.name].schedule.startDate)
      ) {
        planSet[plan.name] = plan;
      }
    } else {
      planSet[plan.name] = plan;
    }
  }

  let filteredPlanList = [];

  for (let key of Object.keys(planSet)) {
    filteredPlanList.push(planSet[key]);
  }

  return filteredPlanList;
};

export const numberToPrice = (number) => {
  return number.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
};
