import NavBar from '../../components/Navbar/NavBar';
import classes from './Layout.module.css';

export default function Layout({ children }) {
  return (
    <>
      <NavBar />
      <main className={classes.wrapper}>{children}</main>
    </>
  );
}
