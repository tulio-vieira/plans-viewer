import classes from './ContainerWrapper.module.css';

export default function ContainerWrapper({ children }) {
  return <div className={classes.ContainerWrapper}>{children}</div>;
}
