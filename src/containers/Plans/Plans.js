import classes from './Plans.module.css';
import { Component } from 'react';
import Plan from '../../components/Plan/Plan';
import data from '../../shared/data.json';
import { filterPlans } from '../../shared/util';
import Skeleton from '../../components/Skeleton/Skeleton';

export default class Plans extends Component {
  state = { loading: true };

  componentDidMount() {
    // fazer request para API e pagar data.json
    // TODO: adicionar tratamento de erros, caso a API falhe ou retorne dados ruins
    setTimeout(() => {
      this.filteredPlanList = filterPlans(data.plans);
      this.setState({ loading: false });
    }, 2000);
  }

  render() {
    return this.state.loading ? (
      <div>
        <h1>
          <Skeleton />
        </h1>
        <Skeleton amount={3} />
      </div>
    ) : (
      <>
        {this.filteredPlanList.map((plan) => (
          <Plan key={plan.id} {...plan} />
        ))}

        {data.observations.map((obs, i) => (
          <div key={i} className={classes.obs}>
            {obs}
          </div>
        ))}
      </>
    );
  }
}
